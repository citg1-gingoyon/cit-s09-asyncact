package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@SpringBootApplication
//This application will function as an endpoint that will be used in handling Http request

@RestController
@RequestMapping("/greeting")
//will require all routs within the class to use the set endpoint as part of its rout
public class DiscussionApplication {

	 List<Student> students = new ArrayList<>();

	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}

	/*//localhost:8080/hello
	@GetMapping("/hello")
	//Maps a get request to the rout "/hello" and invokes the method hello
	public String hello(){
		return "Hello World";
	}

	// Rout with String query
	//localhost:8080/hi?name=value
	@GetMapping("/hi")
	//"@RequestParam" annotation that allows us to extract data from query string
	public String hi(@RequestParam(value="name",defaultValue = "john") String name){
		return String.format("Hi %s",name);
	}
//	Multiple parameter
	// localhost:8080/friend?name=value&friend-value
	@GetMapping("/friend")
	public String friend(@RequestParam(value="name", defaultValue = "Joe")String name,@RequestParam(value="friend", defaultValue = "Jane")String friend){
		return String.format("Hello %s! My name is %s.",friend,name);
	}
//	Rout with path variable
	//Dynamic data is obtained from the url
	//localhost:8080/name
	@GetMapping("/hello/{name}")
	//"@PathVariable annotation allows us to extract data directly from the URL
	public String greetFriend(@PathVariable("name")String name){
		return String.format("Nice to meet you %s!",name);
	}
*/
	// Async Activity S09

	//This code is for  /welcome route that accept multiple query string parameter (user & role)
	@GetMapping("/welcome")
		public String users(@RequestParam(value="role",defaultValue = "admin")String role,@RequestParam(value="user",defaultValue = "diether")String user){

			switch(role){
				//A
				case "admin":
					return String.format("Welcome back to the class portal, Admin %s!",user);

				//B
				case "teacher":
					return  String.format("Thank you for logging in, Teacher %s!",user);

				//C
				case "student":
					return String.format("Welcome to the class portal, %s!",user);

				//D
				default:
					return String.format("Role out of range!");

			}

		}

	//This code is for /register route that will accept multiple query string parameter (id,name, & course)
	//Create a Student class
	public class Student{
			private String id;
			private String name;
			private String course;

		public Student(String id, String name, String course) {
			this.id = id;
			this.name = name;
			this.course = course;
		}

		// getters and setters for id, name, and course

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getCourse() {
			return course;
		}

		public void setCourse(String course) {
			this.course = course;
		}

	}

	@GetMapping("/register")
	public String register(@RequestParam String id,@RequestParam String name, @RequestParam String course){
		Student student = new Student(id,name,course);
		students.add(student);
		return String.format(""+ id +" your id number is registered on the system!");
	}
//	Create a /account/id route that will use Path Variable
	@GetMapping("/account/{id}")
	public String account(@PathVariable("id") String id) {
		// Search for the student with the given id
		Student studentFound=null;
		for (Student student : students) {
			if (student.getId().equals(id)) {
				studentFound=student;
				break;
			}
		}
		if(studentFound!=null){
			return String.format("Welcome back %s! You are currently enrolled in %s",studentFound.getName(),studentFound.getCourse());
		}
		else{
			return String.format("Your provided " +id+ " is not found in the system!");
		}
	}

}
